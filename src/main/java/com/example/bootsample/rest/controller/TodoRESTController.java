package com.example.bootsample.rest.controller;

import com.example.bootsample.entity.Todo;
import com.example.bootsample.service.TodoService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/todo")
public class TodoRESTController {

    private TodoService todoService;

    // @Autowired (省略)
    public TodoRESTController(TodoService todoService) {
        this.todoService = todoService;
    }

    @GetMapping
    public List<Todo> findAll() {
        return todoService.findAll();
    }

    @GetMapping("/{id}")
    public Todo findById(@PathVariable("id") long id) {
        return todoService.findById(id);
    }

    @PostMapping
    public void add(@RequestParam String newTodoText) {
        todoService.add(newTodoText);
    }

    @PutMapping("/{id}")
    public void done(@PathVariable("id") long id) {
        todoService.done(id);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable("id") long id) {
        todoService.remove(id);
    }
}
